# Gestion des Logical Volumes (LVM) et Volume Groups (VG) sur Linux

## Introduction

Le Logical Volume Management (LVM) est une méthode de gestion de l'espace disque qui permet de créer des volumes logiques à partir de plusieurs disques physiques. Cela offre une flexibilité accrue pour la gestion de l'espace de stockage sur un système Linux. Dans ce guide, nous allons explorer la création de Volume Groups (VG), Logical Volumes (LV), ainsi que le redimensionnement d'un LV sous le système de fichiers XFS.

## Terminologie

- **Volume Group (VG)** : Un ensemble de Physical Volumes (PV) regroupés pour créer un pool de stockage.
- **Physical Volume (PV)** : Un disque dur, une partition ou un RAID utilisé pour former un VG.
- **Logical Volume (LV)** : Un volume logique créé à partir d'un VG, utilisé comme partition logique.

## Création d'un Volume Group (VG) et d'un Logical Volume (LV)

1. **Création d'un Volume Group (VG)** : Utilisez la commande suivante pour créer un VG en spécifiant les Physical Volumes (PV).

```bash
vgcreate nom_du_vg /dev/sdX1 /dev/sdX2
```

2. **Création d'un Logical Volume (LV)** : Utilisez la commande suivante pour créer un LV dans le VG spécifié avec une taille donnée.

```bash
lvcreate -L taille_du_lv -n nom_du_lv nom_du_vg
```

## Redimensionnement d'un Logical Volume (LV) sous XFS

1. **Extension de la taille du LV** : Utilisez la commande `lvextend` pour étendre la taille du LV.

```bash
lvextend -L+10G /dev/nom_du_vg/nom_du_lv
```

    Remplacez `10G` par la taille que vous souhaitez ajouter.

2. **Extension de la taille du LV avec +100% d'espace libre** : Utilisez la commande `lvextend` avec l'option `+100%FREE`.

```bash
lvextend -l +100%FREE /dev/nom_du_vg/nom_du_lv
```

    Cette commande utilise tout l'espace libre disponible dans le VG pour étendre le LV.

## Extension avec d'autres formats de système de fichiers

1. **Extension du système de fichiers XFS** : Utilisez la commande `xfs_growfs` pour étendre le système de fichiers XFS après avoir agrandi le LV.

```bash
xfs_growfs /dev/nom_du_vg/nom_du_lv
```

    Cette commande ajuste la taille du système de fichiers XFS pour refléter la nouvelle taille du LV.

2. **Extension du système de fichiers ext4** : Utilisez la commande `resize2fs` pour étendre un système de fichiers ext4 après avoir agrandi le LV.

```bash
resize2fs /dev/nom_du_vg/nom_du_lv
```

3. **Extension du système de fichiers btrfs** : Utilisez la commande `btrfs filesystem resize` pour étendre un système de fichiers btrfs après avoir agrandi le LV.

```bash
btrfs filesystem resize +10G /mnt/point_de_montage
```