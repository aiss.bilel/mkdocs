# Guide de Partage NFS

Le système NFS (Network File System) est un protocole de partage de fichiers qui permet à un système client d'accéder et de monter des répertoires partagés depuis un serveur distant. Ce guide vous fournira des instructions étape par étape pour configurer le partage NFS sur un système Linux.

## 1. Installation de NFS

Assurez-vous d'avoir les paquets nécessaires installés sur le serveur et le client.

### Sur le serveur (exemple pour Ubuntu) :

```bash
sudo apt update
sudo apt install nfs-kernel-server
```

Sur le client (exemple pour Ubuntu) :
```bash
sudo apt update
sudo apt install nfs-common
```
## 2. Configuration du Serveur NFS

Éditez le fichier de configuration du serveur NFS (/etc/exports par défaut).
```bash
sudo nano /etc/exports
```
Ajoutez les répertoires que vous souhaitez partager et les autorisations d'accès.
plaintext
/chemin/vers/repertoire client_ip(rw,sync,no_subtree_check)
Redémarrez le service NFS.
```bash
sudo systemctl restart nfs-kernel-server
```
## 3. Configuration du Client NFS
Aucune configuration particulière n'est nécessaire sur le client. Assurez-vous simplement que le package NFS est installé.

## 4. Montage d'un Répertoire NFS
Créez un répertoire local pour le montage.
```bash
sudo mkdir /mnt/nfs-share
```
Montez le répertoire partagé depuis le serveur.
```bash
sudo mount server_ip:/chemin/vers/repertoire /mnt/nfs-share
```

## 5. Options de Montage NFS
Vous pouvez spécifier des options supplémentaires lors du montage.

```bash
sudo mount -o option1,option2 server_ip:/chemin/vers/repertoire /mnt/nfs-share
```
Par exemple, pour le montage en lecture seule :

```bash
sudo mount -o ro server_ip:/chemin/vers/repertoire /mnt/nfs-share
```
## 6. Sécurité NFS
Restreignez l'accès en spécifiant les adresses IP autorisées dans /etc/exports.
Considérez l'utilisation de mécanismes d'authentification comme Kerberos pour renforcer la sécurité.
## 7. Dépannage
Vérifiez les journaux système pour des erreurs (/var/log/syslog sur Ubuntu).
Utilisez la commande showmount pour afficher les exports du serveur.
```bash
showmount -e server_ip
```