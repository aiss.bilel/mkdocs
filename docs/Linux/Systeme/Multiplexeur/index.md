# Gestion des Sessions Multiplexers : tmux et screen

## Introduction

Les sessions multiplexers sont des outils puissants pour la gestion de sessions et de terminaux multiples dans un environnement UNIX. Deux des sessions multiplexers les plus couramment utilisés sont `tmux` et `screen`. Dans ce guide, nous explorerons les fonctionnalités de base de ces deux outils.

## tmux

### Lancement d'une Session tmux

1. Pour lancer une session tmux, utilisez la commande suivante :
    ```bash
    tmux
    ```

2. Pour rejoindre une session existante, utilisez la commande :
    ```bash
    tmux attach
    ```

### Contrôle de tmux

- **Escape Key** : L'escape key pour tmux est `CTRL+b`.
- **Scrolling dans le Multiplexer** : Pour scroller dans le multiplexer, utilisez `CTRL+b` suivi de `pageup/pagedown`.
- **Détacher la Session sans la Tuer** : Pour détacher la session sans la tuer, utilisez `CTRL+b` suivi de `d`.
- **Rebooter la Machine** : Pour rebooter votre machine depuis tmux, utilisez `CTRL+b` suivi de `:`, puis entrez `reboot` dans la barre de commandes.

## screen

### Lancement d'une Session screen

1. Pour lancer une session screen, utilisez la commande suivante :
    ```bash
    screen
    ```

2. Pour rejoindre une session existante, utilisez la commande :
    ```bash
    screen -r
    ```

### Contrôle de screen

- **Escape Key** : L'escape key pour screen est `CTRL+a`.
- **Scrolling dans le Multiplexer** : Pour scroller dans le multiplexer, utilisez `CTRL+a` suivi de `[` et utilisez les touches de défilement.
- **Détacher la Session sans la Tuer** : Pour détacher la session sans la tuer, utilisez `CTRL+a` suivi de `d`.
- **Rebooter la Machine** : Pour rebooter votre machine depuis screen, utilisez `CTRL+a` suivi de `:`, puis entrez `reboot` dans la barre de commandes.