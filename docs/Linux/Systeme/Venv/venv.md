# Procédure pour créer un environnement virtuel sur Linux en utilisant venv

Un environnement virtuel est un espace isolé où vous pouvez installer des packages Python sans interférer avec le système global. Cela est particulièrement utile lorsque vous travaillez sur plusieurs projets avec des dépendances différentes. Dans cet exemple, nous utiliserons le module `venv` intégré à Python pour créer un environnement virtuel.

## Étape 1 : Vérifier la version de Python

Assurez-vous d'avoir Python installé sur votre système. Ouvrez un terminal et exécutez la commande suivante pour vérifier la version de Python :

```bash
python3 --version
```

## Étape 2 : Installer venv (si nécessaire)
Si le module venv n'est pas déjà installé, vous pouvez l'installer en utilisant la commande suivante :

```bash
sudo apt update
sudo apt install python3-venv
```
## Étape 3 : Créer un Environnement Virtuel avec des Requirements
Choisissez ou créez un répertoire pour votre projet et rendez-vous dans ce répertoire via le terminal. Créez un fichier requirements.txt avec les dépendances de votre projet, puis utilisez la commande suivante pour créer l'environnement virtuel en spécifiant ces requirements :

```bash
python3 -m venv nom_de_lenv --requirements=requirements.txt
```
Remplacez nom_de_lenv par le nom que vous souhaitez donner à votre environnement virtuel.

## Étape 4 : Activer l'Environnement Virtuel
Activez l'environnement virtuel en utilisant la commande suivante :

```bash
source nom_de_lenv/bin/activate
```
Vous verrez le nom de votre environnement virtuel apparaître dans l'invite de commande, indiquant que l'environnement virtuel est maintenant actif.

## Étape 5 : Désactiver l'Environnement Virtuel
Pour désactiver l'environnement virtuel, utilisez la commande suivante :

```bash
deactivate
```
L'invite de commande reviendra au système global.

## Étape 6 : Installer des Packages dans l'Environnement Virtuel
Avec l'environnement virtuel activé, vous pouvez utiliser pip pour installer des packages spécifiques à votre projet sans affecter le système global. Par exemple :

```bash
pip install nom_du_package
```
## Étape 7 : Exporter les Dépendances
Pour exporter les dépendances installées dans votre environnement virtuel, utilisez la commande suivante :

```bash
pip freeze > requirements.txt
```
Cela crée un fichier requirements.txt qui peut être partagé avec d'autres développeurs pour reproduire votre environnement.