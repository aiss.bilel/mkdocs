# Gestion du Keystore Java avec keytool

## Introduction

`keytool` est une commande en ligne pour gérer les keystores Java, qui sont des bases de données utilisées pour stocker des certificats et clés privées. Cette documentation présente quelques opérations courantes telles que la liste, l'ajout, et la suppression de certificats dans un keystore.

## Exemple de Commande

Voici un exemple de commande `keytool` pour importer un certificat dans le keystore par défaut (cacerts) :

```bash
sudo keytool -import -noprompt -alias alias_custom -keystore /usr/lib/jvm/zulu-17/lib/security/cacerts -storepass changeit -file /chemin/vers/le/certificat.pem
```
## Explication des Options

- **-import** : Indique une opération d'importation.
- **-noprompt** : Évite la demande de confirmation lors de l'importation.
- **-alias** : Spécifie un alias pour le certificat.
- **-keystore** : Indique le chemin vers le keystore.
- **-storepass** : Mot de passe pour accéder au keystore (dans cet exemple, le mot de passe par défaut est "changeit").
- **-file** : Chemin vers le certificat à importer.

## Opérations Courantes avec keytool

### Lister les Certificats dans un Keystore

Pour lister les certificats dans un keystore, utilisez la commande suivante :

```bash
keytool -list -keystore /chemin/vers/le/keystore -storepass votre_mot_de_passe
```

## Ajouter un Certificat au Keystore

Pour ajouter un certificat au keystore, utilisez la commande suivante :

```bash
keytool -import -noprompt -alias alias_custom -keystore /chemin/vers/le/keystore -storepass votre_mot_de_passe -file /chemin/vers/le/certificat.pem
```

## Supprimer un Certificat du Keystore

Pour supprimer un certificat du keystore, utilisez la commande suivante :

```bash
keytool -delete -noprompt -alias alias_custom -keystore /chemin/vers/le/keystore -storepass votre_mot_de_passe
```
Modifier le Mot de Passe du Keystore
Pour changer le mot de passe du keystore, utilisez l'option -storepass suivi du nouveau mot de passe :

```bash
keytool -storepasswd -keystore /chemin/vers/le/keystore -storepass ancien_mot_de_passe -new vot
```