# Gestion des Utilisateurs et des Groupes sur Linux

Ce guide vous expliquera comment créer un utilisateur, un groupe, ajouter l'utilisateur au groupe, le supprimer du groupe et attribuer des droits sudo au groupe sur un système Linux.

## 1. Création d'un Utilisateur

Pour créer un nouvel utilisateur, utilisez la commande `adduser`.

```bash
sudo adduser nom_utilisateur
```
## 2. Création d'un Groupe
Utilisez la commande groupadd pour créer un nouveau groupe.

```bash
sudo groupadd nom_du_groupe
```
## 3. Ajout d'un Utilisateur à un Groupe
Utilisez la commande usermod pour ajouter un utilisateur existant à un groupe.

```bash
sudo usermod -aG nom_du_groupe nom_utilisateur
```

Assurez-vous que l'utilisateur se déconnecte et se connecte à nouveau pour appliquer les changements.

## 4. Suppression d'un Utilisateur d'un Groupe
Utilisez la commande gpasswd pour supprimer un utilisateur d'un groupe.

```bash
sudo gpasswd -d nom_utilisateur nom_du_groupe
```
## 5. Attribution des Droits Sudo au Groupe
Éditez le fichier /etc/sudoers avec la commande visudo.

```bash
sudo visudo
```
Ajoutez la ligne suivante pour permettre à un groupe spécifique d'utiliser sudo sans mot de passe.

```
%nom_du_groupe ALL=(ALL:ALL) NOPASSWD: ALL
```
