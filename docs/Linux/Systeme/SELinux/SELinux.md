Relabel selinux au demarrage :fixfiles -F onboot

# Documentation SELinux

## Introduction à SELinux

SELinux (Security-Enhanced Linux) est un système de contrôle d'accès obligatoire (MAC) pour Linux, intégré au noyau Linux. Il renforce la sécurité en appliquant des règles de sécurité strictes et en limitant l'accès des processus aux ressources du système.

## 1. Commandes de Base

### 1.1 Vérifier le statut SELinux

Pour vérifier le statut de SELinux, utilisez la commande suivante :

```bash
sestatus
```
### 1.2 Activer/désactiver SELinux temporairement
Pour activer ou désactiver SELinux temporairement, utilisez la commande setenforce :

```bash
sudo setenforce 0    # Désactiver
sudo setenforce 1    # Activer
```
## 2. Gestion des Politiques SELinux
### 2.1 Lister les modules SELinux installés
La commande suivante liste les modules SELinux installés :

```bash
semodule -l
```

### 2.2 Afficher les règles de sécurité SELinux pour un fichier
Pour afficher les règles de sécurité SELinux pour un fichier, utilisez sesearch :

```bash
sesearch -A -s <source_type> -t <target_type> -c <permission>
```

### 2.3 Afficher les contextes SELinux pour un fichier
Pour afficher les contextes SELinux pour un fichier, utilisez ls avec l'option -Z :

```bash
ls -lZ <fichier>
```
##  3. Modification des Politiques
### 3.1 Ajouter une règle SELinux (temporaire)
Utilisez semanage permissive pour ajouter une règle SELinux temporaire :

```bash
sudo semanage permissive -a <type_domaine>
```
### 3.2 Supprimer une règle SELinux (temporaire)
Utilisez semanage permissive pour supprimer une règle SELinux temporaire :

```bash
sudo semanage permissive -d <type_domaine>
```

##  4. Configuration SELinux
### 4.1 Configurer SELinux
Modifiez le fichier /etc/selinux/config pour configurer SELinux. Changez SELINUX=permissive pour autoriser tout avec journalisation.

### 4.2 Configurer les ports SELinux
Utilisez semanage port pour configurer les ports SELinux :

```bash
semanage port -a -t <type_port> -p <protocole> <port>
```
### 4.3 Lister les ports SELinux autorisés
La commande suivante liste les ports SELinux autorisés :

```bash
semanage port -l
```


## 5 Explication de chcon et semanage fcontext

chcon est utilisé pour changer le contexte de sécurité SELinux d'un fichier ou d'un répertoire. Cependant, ces changements ne sont pas persistants après un redémarrage. Par exemple, pour changer le contexte d'un fichier :

```bash
sudo chcon -t <nouveau_type> <fichier>
```
semanage fcontext est utilisé pour configurer les correspondances entre les chemins de fichiers et les contextes de sécurité SELinux persistants. Cela permet de spécifier comment les contextes SELinux doivent être attribués lors de la création de fichiers ou de répertoires. Par exemple, pour ajouter une correspondance persistante :

```bash
sudo semanage fcontext -a -t <type_cible> <chemin_cible>
```