# Open firewall ports
if (-not (Get-NetFirewallRule -Name "rules1" -ErrorAction SilentlyContinue)) {
    New-NetFirewallRule -DisplayName "TeamForCapellaAcceptor2036" -Direction Inbound -Protocol TCP -LocalPort "2036" -Action Allow -Enabled True
} else {
    Set-NetFirewallRule -Name "TeamForCapellaAcceptor2036" -Direction Inbound -Protocol TCP -LocalPort "2036" -Action Allow -Enabled True
}